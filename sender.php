<?
	$rID = $_POST["registerID"];
	$msg = $_POST["msg"];
	$apiKey = "AIzaSyAAvMHQW00McWOj5YjacsC63NcwhDiy1F4";

	$headers = array('Content-Type:application/json',
                 "Authorization:key=$apiKey");

	$datos = array(
                'registration_ids'  => array($rID),
                'data'              => array( "message" => $msg )
        );


	$post = curl_init();

	curl_setopt($post, CURLOPT_HTTPHEADER, $headers);

	curl_setopt($post, CURLOPT_URL, "https://android.googleapis.com/gcm/send");

	curl_setopt($post, CURLOPT_POST, true);

	curl_setopt( $post, CURLOPT_SSL_VERIFYHOST, 0 );
	
	curl_setopt( $post, CURLOPT_SSL_VERIFYPEER, 0 );

	curl_setopt($post, CURLOPT_RETURNTRANSFER, true);

	curl_setopt($post, CURLOPT_POSTFIELDS, json_encode($datos));

	$result = curl_exec($post);

	curl_close($post);

	echo $result;

	//echo json_encode($datos);
